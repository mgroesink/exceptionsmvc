#Exception Handling in ASP.NET / MVC
##Source code in C-Sharp

In dit Visual Studio project wordt gedemonstreerd hoe je fouten correct af kunt handelen, zodat je applicatie niet crasht als zich een fout voordoet.

Er wordt een class library toegevoegd, waarin een eigen Exception class wordt gemaakt, die gebruikt kan worden als een onjuist e-mailadres wordt toegevoegd aan een studenten obejct.
Daarbij wordt gebruik gemaakt van een regular expression om het e-mailadres te valideren.

De master branch bevat alleen nog maar:

1.  Een model voor een student met 3 properties: FirstName, LastName, Email
2.  Een controller met CRUD actions voor een Student, waarvan alleen Create is geīmplementeerd
3.  Een view om een Student aan te maken
4.  Een regular expression om een email te controleren op basis van [RFC 2822](https://en.wikipedia.org/wiki/Email#Internet_Message_Format)

Middels een try ... catch wordt een Exception gegenereerd als het e-mailadres ongeldig is (in de setter van de class Student), maar de fout wordt nog niet afgehandeld.

=======================================================================================================
###UPDATE
De code is inmiddels uitgebreid en het voorbeeld is afgerond.

Er is een class ibrary toegevoegd (MyFunctions) met daarin een eigen class InvalidEmailException.

In de StudentController wordt de fout afgehandeld en een foutmelding wordt via de ViewBag doorgegeven naar de View. Ook worden de eerder ingevoerde gegevens dorogestuurd naar de View, zodat de gebruiker niet opnieuw hoeft te beginnen met invoeren.
