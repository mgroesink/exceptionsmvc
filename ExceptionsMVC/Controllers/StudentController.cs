﻿using ExceptionsMVC.Models;
using MyFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExceptionsMVC.Controllers
{

    public class StudentController : Controller
    {
        static List<Student> students = new List<Student>();
        // GET: Student
        public ActionResult Index()
        {
            
            return View(students);
        }

        // GET: Student/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Student s = new Student();

                s.FirstName = collection["FirstName"];
                s.LastName = collection["LastName"];
                s.Email = collection["Email"];
                students.Add(s);
                return RedirectToAction("Index");
            }
            catch(InvalidEmailExeption ex)
            {
                ViewBag.FirstName = collection["FirstName"];
                ViewBag.LastName = collection["LastName"];
                ViewBag.Email = collection["Email"];
                ViewBag.ErrorMessage = ex.ErrorMessage;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.FirstName = collection["FirstName"];
                ViewBag.LastName = collection["LastName"];
                ViewBag.Email = collection["Email"];
                ViewBag.ErrorMessage = "Ongeldig emailadres";
                return View();
            }
            finally
            {

            }
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Student/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
