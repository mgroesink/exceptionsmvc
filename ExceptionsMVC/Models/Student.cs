﻿using MyFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using System.Web;

namespace ExceptionsMVC.Models
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        private string email;

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                // Controleren of het een geldig emailadres is
                bool isEmail = Regex.IsMatch(value, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", 
                    RegexOptions.IgnoreCase);
                if(isEmail)
                {
                    email = value;
                }
                else
                {
                    throw new InvalidEmailExeption("Ongeldig Emailadres", 1);
                }
            }
        }
    }
}