﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFunctions
{
    public class InvalidEmailExeption :Exception
    {
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

        public InvalidEmailExeption(string message , int code)
        {
            this.ErrorMessage = message;
            this.ErrorCode = code;
        }
    }
}
